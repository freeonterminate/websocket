# WebSocket For Delphi 

# PLATFORMS
Windows / macOS / Linux (Maybe iOS, Android)

# ENVIRONMENT
Delphi 10.3.x Rio and up.  

# REQUIRED LIBRARY
OpenSSL Files  

| OS      | Library 1       | Library 2    |  
|---------|-----------------|--------------|  
| Windows | libeay32.dll    | ssleay32.dll |  
| macOS   | libcrypto.dylib | libssl.dylib |  
| iOS     | libcrypto.a     | libssl.a     |  
| Android | libcrypto.so    | libssl.so    |  
| Linux   | libcrypto.so    | libssl.so    |  

**See also below URL  **  
http://docwiki.embarcadero.com/RADStudio/Rio/en/Securing_Indy_Network_Connections  
http://docwiki.embarcadero.com/RADStudio/Rio/en/OpenSSL  
https://indy.fulgan.com/SSL/  

# LICENSE
  Copyright (C) 2019 HOSOKAWA Jun (Twitter: @pik)  
  Released under the MIT license  
  http://opensource.org/licenses/mit-license.php  

# HISTORY
2019/04/04 Version 1.0.0  
2019/04/11 Version 1.0.1 Change CloseEvent  
2019/04/12 Version 1.0.2 Fixed Closing Bug  
2019/04/15 Version 1.0.3 Fixed Reconnect Bug  
2019/04/17 Version 1.0.4 Fixed Log Bug  
2019/04/19 Version 1.0.5 Added ResetHeartbeat  
2019/04/21 Version 1.0.6 Added BytesToString, BytesToHex  
2019/04/24 Version 1.0.7 Fixed BytesToHex Hints  
2019/06/06 Version 1.0.8 Fixed Change SocketClosed Methods
2020/03/19 version 1.0.9 Fixed Not use SSL Connection

# USAGE

```delphi
// Create
procedure TfrmMain.FormCreate(Sender: TObject);
begin
  FWebSocket := TWebSocket.Create(Self);
  FWebSocket.OnError := WebSocketError;
  FWebSocket.OnText := WebSocketText;
end;

// Destroy
procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  FWebSocket.DisposeOf;
end;

// Connect
procedure TfrmMain.Button1Click(Sender: TObject);
begin
  FWebSocket.Connect('wss://echo.websocket.org');
end;

// Send Text
procedure TfrmMain.Button2Click(Sender: TObject);
begin
  if FWebSocket.Connected then
    FWebSocket.Send('Hello, WebSocket !');
end;

// Established Connection.
// After the connection is established, communication becomes possible
procedure TfrmMain.WebSocketEstablished(Sender: TObject);
begin
  Memo1.Lines.Add('Estalished !');
end;

// ReceiveText from Server
procedure TfrmMain.WebSocketText(Sender: TObject; const iText: String);
begin
  Memo1.Lines.Add('RECEIVED: ' + iText);
end;

// Error Handler
procedure TfrmMain.WebSocketError(
  Sender: TObject;
  const AErrorCode: TSSLSocketErrCode;
  const AException: Exception);
begin
  Memo1.Lines.Add(Format('ERROR: %d  %s', [AErrCode, AException.Message]));
end;
```

